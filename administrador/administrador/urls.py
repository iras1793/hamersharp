"""administrador URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from dashboard import views as dashboard_views

urlpatterns = [
    path('admin/', admin.site.urls),

    #------------------------------------------VISTAS DE LOGIN----------------------------------------------------------
    path('', dashboard_views.login, name='login'),
    path('login/', dashboard_views.login, name='login'),
    path('logout/', dashboard_views.logout, name='logout'),
    #------------------------------------------FIN VISTAS DE LOGIN------------------------------------------------------

    #------------------------------------------VISTAS DE DASHBOARD------------------------------------------------------
    path('dashboard/', dashboard_views.dashboard, name='dashboard'),
    path('eventos/', dashboard_views.eventos, name='eventos'),
    #------------------------------------------FIN VISTAS DE DASHBOARD--------------------------------------------------

    #------------------------------------------VISTAS DE ALUMNOS--------------------------------------------------------
    path('alumnos/', dashboard_views.alumnos, name='alumnos'),
    path('alumnos/ver/', dashboard_views.alumnosView, name='alumnosview'),
    path('alumnos/agregar/', dashboard_views.alumnosInsert, name='alumnosinsert'),
    path('alumnos/editar/<int:id>', dashboard_views.alumnosUpdate, name='alumnosupdate'),
    path('alumnos/eliminar/<id>/', dashboard_views.alumnosDelete, name='alumnosDelete'),
    #------------------------------------------FIN VISTAS DE ALUMNOS----------------------------------------------------

    #------------------------------------------VISTAS DE PROFESORES-----------------------------------------------------
    path('profesores/', dashboard_views.profesores, name='profesores'),
    path('profesores/agregar/', dashboard_views.profesoresInsert, name='profesoresinsert'),
    path('profesores/eliminar/<id>/', dashboard_views.profesoresDelete, name='profesoresdelete'),
    path('profesores/editar/<id>/', dashboard_views.profesoresUpdate, name='profesoresupdate'),
    #------------------------------------------FIN VISTAS DE PROFESORES-------------------------------------------------

    #------------------------------------------VISTAS DE GRUPOS---------------------------------------------------------
    path('grupos/', dashboard_views.grupos, name='grupos'),
    path('grupos/agregar', dashboard_views.gruposInsert, name='gruposinsert'),
    path('grupos/editar/<id>', dashboard_views.gruposUpdate, name='gruposUpdate'),
    path('grupos/eliminar/<id>/', dashboard_views.gruposDelete, name='gruposDelete'),
    path('grupos/asistencia/<id>', dashboard_views.asistencia, name='gruposAsistencia'),
    #-- CLASES
    path('clases/agregar/<id>/', dashboard_views.clasesInsert, name='clasesinsert'),
    path('clases/insertar/', dashboard_views.clasesInsertar, name='clasesinsertar'),
    path('clases/', dashboard_views.clases, name='clases'),
    #------------------------------------------FIN VISTAS DE GRUPOS-----------------------------------------------------

    #------------------------------------------VISTAS DE CILCO ESCOLAR--------------------------------------------------
    path('cicloescolar/', dashboard_views.cicloescolar, name='cicloescolar'),
    path('cicloescolar/historico/', dashboard_views.cicloescolarHistorico, name='cicloescolarhistorico'),
    path('cicloescolar/ver/<id>/', dashboard_views.cicloescolarView, name='cicloescolarview'),
    path('cicloescolar/agregar/', dashboard_views.cicloescolarInsert, name='cicloescolarinsert'),
    path('cicloescolar/editar/', dashboard_views.cicloescolarUpdate, name='cicloescolarupdate'),
    path('cicloescolar/eliminar/', dashboard_views.cicloescolarDelete, name='cicloescolardelete'),
    #------------------------------------------FIN VISTAS DE CILCO ESCOLAR----------------------------------------------

    #------------------------------------------VISTAS DE SUCURSALES-----------------------------------------------------
    path('sucursales/', dashboard_views.sucursales, name='sucursales'),
    path('sucursales/agregar/', dashboard_views.sucursalesInsert, name='sucursalesinsert'),
    path('sucursales/editar/<int:id>/', dashboard_views.sucursalesUpdate, name='sucursalesUpdate'),
    path('sucursales/eliminar/<int:id>/', dashboard_views.sucursalesDelete, name='sucursalesDelete'),
    #------------------------------------------FIN VISTAS DE SUCURSALES-------------------------------------------------

    #------------------------------------------VISTAS DE REPORTES-------------------------------------------------------
    path('reportes/historico/', dashboard_views.historico, name='historico'),
    path('reportes/', dashboard_views.reporte, name='reportes'),
    #------------------------------------------VISTAS DE REPORTES-------------------------------------------------------

    #------------------------------------------VISTAS DE GENERAR EXCEL--------------------------------------------------
    path('generarExcel/', dashboard_views.generarExcel, name="generarExcel"),
    path('generar/<id>/', dashboard_views.generar, name="generar"),
    #------------------------------------------FIN VISTAS DE GENERAR EXCEL----------------------------------------------

    #------------------------------------------VISTAS DE PAGOS Y FACUTAS------------------------------------------------
    path('pagos/profesores/',dashboard_views.profesoresPagos,name="profesorespagos"),
    path('pagos/profesores/agregar/<int:id>', dashboard_views.pagosProfesorAgregar,name="pagosagregarprofesores"),
    path('pagos/profesores/historial/<int:id>', dashboard_views.pagosProfesorHistorial,name="pagosprofesoreshistorial"),
    path('pagos/alumnos', dashboard_views.listaGrupo, name='listagrupo'),
    path('pagos/alumnos/<id>', dashboard_views.pagoAlumnos, name='pagoalumnos'),
    #--FACTURACION
    path('facturacion/<id>', dashboard_views.facturacion, name='facturacion'),
    path('compraMaterial/<id>', dashboard_views.compraMaterial, name='compraMaterial'),
    path('materialInsert/<id>', dashboard_views.materialInsert, name='materialInsert'),
    path('colegiaturaInsert/<id>', dashboard_views.colegiaturaInsert, name='colegiaturaInsert'),
    path('colegiatura/<id>', dashboard_views.colegiatura, name='pagoColegiatura'),
    #------------------------------------------FIN VISTAS DE PAGOS Y FACUTAS--------------------------------------------
    #path('/', ),
]
from django.conf.urls.static import static
from django.conf import settings


if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
