from ..models import *
from datetime import datetime
from django.http import HttpResponse
from tempfile import NamedTemporaryFile
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.drawing.image import Image
from openpyxl.styles import Font, Border, Alignment, Side, PatternFill
import os

fechaAct=datetime.now()
#-----------------------------------------------------------------------------------------------------------------------------#
def renombrar(**kwargs):
    try:
        auxiliar=kwargs['imagen'].name.split(".")
        size=len(auxiliar)
        #{nuevo nombre}_ _{año}{mes}{dia}.(dot) {extencion}
        nuevo_nombre="dump_{0}_{1}{2}{3}.{4}".format(kwargs['id'],fechaAct.year,fechaAct.month,fechaAct.day,auxiliar[size-1])
        print("-----------------------------------------------------------")
        print(nuevo_nombre)
        print("-----------------------------------------------------------")
        kwargs['imagen'].name=nuevo_nombre
    except Exception as ex:
        print("Excepcion en servicios renombrar {}".format(type(ex).__name__))
    else:
        print("Sin excepciones en vista login")
    finally:
        print("termina metodo de renombrar en servicios")
        return kwargs['imagen']
#-----------------------------------------------------------------------------------------------------------------------------#
def validarSizeImg(tamanio, nombre):
    valido = False
    try:
        nombre_img = nombre.name
        auxiliar = nombre_img.split(".")
        centinela = len(auxiliar)
        size = tamanio
        extencion = auxiliar[centinela-1]
        print("\n\n{}\n\n".format(size))
        extencionesValidas=["jpg","jpeg","png"]
        limit_in_MB = 5242880
        if size < limit_in_MB and extencion in extencionesValidas:
            valido = True
    except Exception as ex:
        print("Excepcion en servicios renombrar {}".format(type(ex).__name__))
    else:
        print("Termina metodo validarSizeImg en vistas sin ninguna excepcion")
    return valido
#-----------------------------------------------------------------------------------------------------------------------------#
def generarHoja(**kwargs):
    idgrupo=kwargs['id']
    print("--------------------------")
    print(idgrupo)
    g=Grupos.objects.get(idgrupo=idgrupo)
    nombreProfesor="{0} {1} {2}".format(g.idprofesor.nombres,g.idprofesor.apellido_paterno,g.idprofesor.apellido_materno)
    print("--------------------------")
    wb = Workbook()
    nombreArchivo="Grupo.xlsx"
    #wb.load_workbook(file=nombreArchivo)
    ws=wb.active
    ws.title = "{0}".format(g.nombre)
    thin = Side(border_style="thin", color="000000")
    border = Border(top=thin, left=thin, right=thin, bottom=thin)
    centrarTexto = Alignment(horizontal="center", vertical="center")
    # Fila 3
    ws['AH3']="NUMERO DE GRUPOS"
    ws['AH3'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AI3']=23
    ws['AI3'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    # Fila 4
    nAlumnos=Alumnos.objects.filter(idgrupo=idgrupo).count()
    ws['AH4']="NUMERO DE ALUMNOS"
    ws['AH4'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AI4']=nAlumnos
    ws['AI4'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    #Fila 7
    """img = Image('hamersharp.png')
    ws.add_image(img, 'A7')"""
    path_to_image = os.path.abspath('dashboard/static/img/logo-excel.png')
    img = Image(path_to_image)
    img.anchor = 'A7'
    ws.add_image(img)
    ws['A7'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)

    #Fila 9
    ws['B9']="{0}".format(g.nombre)
    ws.merge_cells('D9:F9')
    ws['D9']="PROFESOR"
    ws['D9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['E9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['F9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('G9:L9')
    ws['G9']=nombreProfesor
    ws['G9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['H9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['I9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['J9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['K9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['L9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('M9:P9')
    ws['M9']="CICLO"
    ws['M9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['N9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['O9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['P9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('Q9:S9')
    ws['Q9']="NIVEL"
    ws['Q9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['R9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['S9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('T9:X9')
    ws['T9']="PERPS.1"
    ws['T9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['U9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['V9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['W9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['X9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('Y9:AC9')
    ws['Y9']="HORARIO"
    ws['Y9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['Z9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AA9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AB9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AC9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AD9:AG9')
    ws['AD9']="CALENDARIO"
    ws['AD9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AE9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AF9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AG9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AH9']="TOTAL"
    ws['AH9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AI9']="%RI"
    ws['AI9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AJ9']="AI CLC"
    ws['AJ9'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    #Fila 10
    ws.merge_cells('D10:F10')
    ws['D10']="TR"
    ws['D10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['E10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['F10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('G10:L10')
    ws['G10']="70"
    ws['G10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['H10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['I10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['J10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['K10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['L10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('M10:P11')
    ws['M10']=g.idciclo.nombre#Si se pone C03-2019 creo que hace referencia a una resta
    ws['M10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['N10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['O10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['P10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['M11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['N11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['O11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['P11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('Q10:S10')
    ws['Q10']=""
    ws.merge_cells('T10:X10')
    ws['T10']=""
    ws.merge_cells('Y10:AC11')
    ws['Y10']=g.horario
    ws['Y10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['Z10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AA10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AB10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AC10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['Y11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['Z11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AA11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AB11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AC11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AD10:AE10')
    ws['AD10']="INICIA:"
    ws['AD10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AE10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AF10:AG10')
    ws['AF10']=g.inicia
    ws['AF10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AG10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AH10:AH11')
    ws['AH10']="9"
    ws['AH10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AH11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AI10:AI11')
    ws['AI10']=""
    ws['AI10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['ADI11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AJ10:AJ11')
    ws['AJ10']="0"
    ws['AJ10'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AJ11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    #Fila 11
    ws.merge_cells('D11:F11')
    ws['D11']="TH"
    ws['D11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['E11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['F11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('G11:L11')
    ws['G11']="20"
    ws['G11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['H11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['I11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['J11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['K11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['L11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('Q11:S11')
    ws['Q11']="MODULO"
    ws['Q11'].border=Border(bottom=border.bottom)
    ws['R11'].border=Border(bottom=border.bottom)
    ws['S11'].border=Border(bottom=border.bottom)
    ws.merge_cells('T11:X11')
    ws['T11']="1"
    ws['T11'].border=Border(bottom=border.bottom)
    ws['U11'].border=Border(bottom=border.bottom)
    ws['V11'].border=Border(bottom=border.bottom)
    ws['W11'].border=Border(bottom=border.bottom)
    ws['X11'].border=Border(bottom=border.bottom)
    ws.merge_cells('AD11:AE11')
    ws['AD11']="TERMINA:"
    ws['AD11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AE11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells('AF11:AG11')
    ws['AF11']=g.termina
    ws['AF11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AG11'].border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws['AI11'].border=Border(bottom=border.bottom)
    #Fila 12 vacia
    #Fila 13 Este apartado es para los cursos SABATINOS
    ws['A13']="Nombre del Alumno"
    ws['B13']="Telefono"
    ws['C13']="AL"
    ws.merge_cells('D13:H14')
    ws['D13']="16"
    ws.merge_cells('J13:N14')
    ws['J13']="23"
    ws.merge_cells('P13:T14')
    ws['P13']="30"
    ws.merge_cells('V13:Z14')
    ws['v13']="6"
    ws['AA13']="EX1"
    ws['AB13']="EX2"
    ws['AC13']="CCLASE"
    ws['AD13']="FINAL"
    ws['AE13']=" "
    ws['AF13']="No. Factura"
    ws['AG13']="Suma de"
    ws['AH13']="No. Factura"
    ws['AI13']="Suma de"
    ws.merge_cells('AJ13:AJ14')
    ws['AJ13']="ING TOT"
    #Fila 14
    ws['AF14']="CSOS"
    ws['AG14']="Montos CSOS"
    ws['AH14']="Material"
    ws['AI14']="Montos Mat"
    #Inicia for para datos del alumno
    totalAlumnos=Alumnos.objects.filter(idgrupo=idgrupo).count()
    inicio=15
    for alumno in Alumnos.objects.filter(idgrupo=idgrupo):
        nombreCompleto="{0} {1} {2}".format(alumno.nombres,alumno.apellido_paterno,alumno.apellido_materno)
        _ = ws.cell(column=1, row=inicio, value=nombreCompleto).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=2, row=inicio, value=alumno.telefono).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=3, row=inicio, value=1).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        ws.merge_cells(start_row=inicio, start_column=4, end_row=inicio, end_column=8)
        ws.merge_cells(start_row=inicio, start_column=10, end_row=inicio, end_column=14)
        ws.merge_cells(start_row=inicio, start_column=16, end_row=inicio, end_column=20)
        ws.merge_cells(start_row=inicio, start_column=22, end_row=inicio, end_column=26)
        for i in range(4,27):
            _ = ws.cell(column=i, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=27, row=inicio, value=alumno.examen_1).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=28, row=inicio, value=alumno.examen_2).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=29, row=inicio, value=alumno.examen_3).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=30, row=inicio, value=alumno.calificacion_final).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=32, row=inicio, value=alumno.numero_factura_csos).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=33, row=inicio, value=alumno.suma_montos_csos).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=34, row=inicio, value=alumno.numero_factura_material).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=35, row=inicio, value=alumno.suma_montos_csos).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=36, row=inicio, value=alumno.ingreso_total).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        inicio+=1
    inicio+=1 #Alumnos NI
    _ = ws.cell(column=1, row=inicio, value="Alumnos NI")
    inicio+=1#Tabla alumnos NI
    _ = ws.cell(column=1, row=inicio, value="Nombre del alumno").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="Telefono").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=3, row=inicio, value="AL").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=4, end_row=inicio, end_column=8)
    _ = ws.cell(column=4, row=inicio, value="S1").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=5, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=6, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=7, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=8, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
    _ = ws.cell(column=9, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=10, end_row=inicio, end_column=14)
    _ = ws.cell(column=10, row=inicio, value="S2").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=11, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=12, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=13, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=14, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
    _ = ws.cell(column=15, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=16, end_row=inicio, end_column=20)
    _ = ws.cell(column=16, row=inicio, value="S3").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=17, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=18, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=19, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=20, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
    _ = ws.cell(column=21, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=22, end_row=inicio, end_column=26)
    _ = ws.cell(column=22, row=inicio, value="S4").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=23, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=24, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=25, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=26, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
    _ = ws.cell(column=27, row=inicio).border=Border(top=border.top,bottom=border.bottom)
    _ = ws.cell(column=27, row=inicio, value="EX1").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=28, row=inicio, value="EX2").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=29, row=inicio, value="CClase").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=30, row=inicio, value="Final").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=32, row=inicio, value="No. Factura").border=Border(top=border.top,left=border.left,right=border.right)
    _ = ws.cell(column=33, row=inicio, value="Suma de").border=Border(top=border.top,left=border.left,right=border.right)
    _ = ws.cell(column=34, row=inicio, value="No. Factura").border=Border(top=border.top,left=border.left,right=border.right)
    _ = ws.cell(column=35, row=inicio, value="Suma de").border=Border(top=border.top,left=border.left,right=border.right)
    _ = ws.cell(column=36, row=inicio, value="ING TOT").border=Border(top=border.top,left=border.left,right=border.right)
    inicio+=1#fila
    _ = ws.cell(column=32, row=inicio, value="CSOS").border=Border(left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=33, row=inicio, value="Montos CSOS").border=Border(left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=34, row=inicio, value="").border=Border(left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=35, row=inicio, value="Montos Mat").border=Border(left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=36, row=inicio, value="").border=Border(left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1#fila
    for i in range(0,5):
        _ = ws.cell(column=1, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=2, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=3, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        ws.merge_cells(start_row=inicio, start_column=4, end_row=inicio, end_column=8)
        _ = ws.cell(column=4, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=5, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=6, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=7, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=8, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
        _ = ws.cell(column=9, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        ws.merge_cells(start_row=inicio, start_column=10, end_row=inicio, end_column=14)
        _ = ws.cell(column=10, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=11, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=12, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=13, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=14, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
        _ = ws.cell(column=15, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        ws.merge_cells(start_row=inicio, start_column=16, end_row=inicio, end_column=20)
        _ = ws.cell(column=10, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=16, row=inicio).border=Border(top=border.top,bottom=border.bottom,left=border.left)
        _ = ws.cell(column=17, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=18, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=19, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=20, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
        _ = ws.cell(column=21, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=22, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        ws.merge_cells(start_row=inicio, start_column=22, end_row=inicio, end_column=26)
        _ = ws.cell(column=23, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=24, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=25, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=26, row=inicio).border=Border(top=border.top,bottom=border.bottom,right=border.right)
        _ = ws.cell(column=27, row=inicio).border=Border(top=border.top,bottom=border.bottom)
        _ = ws.cell(column=22, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=27, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=28, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=29, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=30, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=32, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=33, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=34, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=35, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        _ = ws.cell(column=36, row=inicio, value="").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
        inicio+=1
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="TIC").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ 2.172,41").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=3, end_row=inicio, end_column=35)
    _ = ws.cell(column=3, row=inicio, value="Notas").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    aux=inicio+7
    ws.merge_cells(start_row=inicio, start_column=3, end_row=aux, end_column=35)
    inicio+=1
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="TEN").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ 1.400,00").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="UO").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ 772,00").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="TIM").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ -").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="TEM").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ -").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="UOM").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ -").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="UOT").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="$ -").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=2, row=inicio, value="36%").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="(o) Asistencia").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="(/) Retardado").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=4, end_row=inicio, end_column=5)
    _ = ws.cell(column=4, row=inicio, value="Mat A").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=5, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=6, end_row=inicio, end_column=8)
    _ = ws.cell(column=6, row=inicio, value="$ 750,00").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=7, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=8, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    aux=inicio+3
    ws.merge_cells(start_row=inicio, start_column=28, end_row=aux, end_column=33)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="(V) Vacaciones").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="(I) Incapacidad").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=4, end_row=inicio, end_column=5)
    _ = ws.cell(column=4, row=inicio, value="Mat B").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=5, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    ws.merge_cells(start_row=inicio, start_column=6, end_row=inicio, end_column=8)
    _ = ws.cell(column=6, row=inicio, value="$ 434,70").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=7, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=8, row=inicio).border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="(A) Ausencia Alumno").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="(R) Rep CLASE").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    inicio+=1
    _ = ws.cell(column=1, row=inicio, value="(M) Ausencia Profesor").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)
    _ = ws.cell(column=2, row=inicio, value="(F) Dia Festivo").border=Border(top=border.top,left=border.left,right=border.right,bottom=border.bottom)

    inicio+=1
    ws.merge_cells(start_row=inicio, start_column=28, end_row=inicio, end_column=33)
    _ = ws.cell(column=28, row=inicio, value="Firma del profesor")


    #for row in range(15,totalAlumnos):
    #    _ = ws.cell(column=1, row=row, value="Jorge Antonio Almazan Mendez")
    ws.column_dimensions["A"].width = 40
    ws.column_dimensions["B"].width = 20
    ws.column_dimensions["C"].width = 3
    ws.column_dimensions["D"].width = 5
    ws.column_dimensions["E"].width = 5
    ws.column_dimensions["F"].width = 5
    ws.column_dimensions["G"].width = 5
    ws.column_dimensions["H"].width = 5
    ws.column_dimensions["I"].width = 2
    ws.column_dimensions["J"].width = 5
    ws.column_dimensions["K"].width = 5
    ws.column_dimensions["L"].width = 5
    ws.column_dimensions["M"].width = 5
    ws.column_dimensions["N"].width = 5
    ws.column_dimensions["O"].width = 2
    ws.column_dimensions["P"].width = 5
    ws.column_dimensions["Q"].width = 5
    ws.column_dimensions["R"].width = 5
    ws.column_dimensions["S"].width = 5
    ws.column_dimensions["T"].width = 5
    ws.column_dimensions["U"].width = 2
    ws.column_dimensions["V"].width = 5
    ws.column_dimensions["W"].width = 5
    ws.column_dimensions["X"].width = 5
    ws.column_dimensions["Y"].width = 5
    ws.column_dimensions["Z"].width = 5
    ws.column_dimensions["AA"].width = 13
    ws.column_dimensions["AB"].width = 13
    ws.column_dimensions["AC"].width = 13
    ws.column_dimensions["AD"].width = 13
    ws.column_dimensions["AE"].width = 2
    ws.column_dimensions["AF"].width = 30
    ws.column_dimensions["AG"].width = 25
    ws.column_dimensions["AH"].width = 30
    ws.column_dimensions["AI"].width = 20
    ws.column_dimensions["AJ"].width = 20



    """Fila 17 Este apartado es para los cursos semanales
    ws['A17']="Nombre del Alumno"
    ws['B17']="Telefono"
    ws['C17']="AL"
    ws['D17']="L1"
    ws['E17']="M1"
    ws['F17']="M1"
    ws['G17']="J1"
    ws['H17']="V1"
    ws['I17']=" "
    ws['J17']="L1"
    ws['K17']="M1"
    ws['L17']="M1"
    ws['M17']="J1"
    ws['N17']="V1"
    ws['O17']=" "
    ws['P17']="L1"
    ws['Q17']="M1"
    ws['R17']="M1"
    ws['S17']="J1"
    ws['T17']="V1"
    ws['U17']=" "
    ws['V17']="L1"
    ws['W17']="M1"
    ws['X17']="M1"
    ws['Y17']="J1"
    ws['Z17']=V1"""
    response = HttpResponse(content_type = "application/excel")
    content="attachment; filename={0}".format(nombreArchivo)
    response['Content-Disposition']=content
    wb.save(response)
    return response
