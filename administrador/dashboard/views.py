# -*- coding: utf-8 -*-
import os
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as autlogin, logout as autlogout
from django.http import HttpResponseRedirect, JsonResponse
from django.core.exceptions import *
from .models import Profesores,Alumnos,Grupos,Cicloescolar,Clases, Sucursales,Materiales, Colegiaturas, PagoProfesores
from .services import servicios
from django.utils import timezone
from django.contrib import messages
from datetime import datetime
from random import randint, random
from django.db.models import Q, Count,  F
from django.http import JsonResponse
import pytz
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------Variables Globales------------------------------------------------------------#
CDMX_TZ = pytz.timezone('America/Mexico_City')
DIR_MEDIA_ALUMNOS = TEMPLATES_PATH = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '../', 'media'))
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------Vistas------------------------------------------------------------------#
@csrf_protect
def login(request):
    data = {}
    try:
        if request.POST:
            usuario = request.POST['usuario']
            password = request.POST['password']
            usuarioEncontrado = User.objects.get(email=usuario) if '@' in usuario else User.objects.get(username=usuario)
            validPassword = usuarioEncontrado.check_password(password)
            if validPassword:
                autenticado = authenticate(username=usuarioEncontrado.username, password=password)
                if autenticado.is_active:
                    autlogin(request, autenticado)
                    return HttpResponseRedirect('/dashboard/')
                else:
                    data['mensaje'] = 'La cuenta de este usuario aún no se ha activado'
            else:
                data['mensaje'] = 'La contraseña no corresponde a ese usuario'
    except ObjectDoesNotExist:
        print("Excepcion en vista login: ObjectDoesNotExist")
        data['mensaje'] = "El correo no existe" if '@' in usuario else 'El usuario no existe'
    except Exception as ex:
        print("Excepcion en vista login: {}".format(type(ex).__name__))
    else:
        print("Sin excepciones en vista login")
    finally:
        print("Termina vista login")
    return render(request, 'login.html', data,)
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def logout(request):
    data = {}
    data['mensaje'] = 'Vuelve pronto'
    autlogout(request)
    return render(request, 'login.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def dashboard(request):
    data = {}
    data['pagina'] = 'dashboard'
    data['alumnos'] = Alumnos.objects.filter(status=1).count()
    data['profesores'] = Profesores.objects.filter(status=1).count()
    data['grupos'] = Grupos.objects.filter(status=1).count()
    data['titulo'] = 'Inicio - Dashboard'
    data['icono'] = 'fa fa-calendar'
    return render(request, 'inicio.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolar(request):
    data = {}
    data['pagina'] = 'cicloescolar'
    data['titulo'] = 'Ciclo escolar'
    data['icono'] = 'fa fa-calendar'
    data['gruposActivos'] = Grupos.objects.select_related().filter(status=1)
    if (Cicloescolar.objects.filter(status=1).exists()):
        data['existe'] = 'verdadero'
    else:
        data['existe'] = 'falso'
    return render(request, 'calendarioEscolar.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarView(request, id):
    data = {}
    data['ciclo_info'] = Cicloescolar.objects.get(idciclo=id)
    return render(request, 'calendarioEscolarVer.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarHistorico(request):
    data = {}
    data['cicloEscolar'] = Cicloescolar.objects.exclude(status=1)
    return render(request, 'historicoCalendarioEscolar.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarInsert(request):
    data = {}
    data['action'] = 'agregar'
    data['titulo'] = 'Inicio - Dashboard'
    data['icono'] = 'fa fa-calendar'
    data['pagina'] = 'cicloescolar'
    data['titulo'] = 'Ciclo escolar'
    data['icono'] = 'fa fa-calendar'
    data['mostrar'] = False
    try:
        if request.method == 'POST':
            nombre = request.POST.get('nombre')
            fechainicio = request.POST.get('fechainicio')
            fechafin = request.POST.get('fechafin')
            if (Cicloescolar.objects.filter(status=1).exists()):
                cicloToEdit = Cicloescolar.objects.get(status=1)
                cicloToEdit.status = 0
                cicloToEdit.save()
            Cicloescolar.objects.create(nombre=nombre, fecha_inicio=fechainicio, fecha_fin=fechafin)
            data['mensaje'] = 'Se agrego el registro correctamente'
            data['mostrar'] = True
            data['estilo'] = 'alert alert-success'

    except Exception as ex:
        print("\nExcepcion en cicloescolarInsert: {0}".format(type(ex).__name__))
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarUpdate(request):
    data = {}
    data['action'] = 'editar'
    data['pagina'] = 'cicloescolar'
    data['titulo'] = 'Ciclo escolar'
    data['icono'] = 'fa fa-calendar'
    try:
        if (Cicloescolar.objects.filter(status=1).exists()):
            data['ciclo_info'] = Cicloescolar.objects.get(status=1)
            ciclo_info = Cicloescolar.objects.get(status=1)
            fechaInicio = str(ciclo_info.fecha_inicio)
            fechaFin = str(ciclo_info.fecha_fin)
            data['fechaInicio'] = fechaInicio[0:10]
            data['fechaFin'] = fechaFin[0:10]
            id = ciclo_info.idciclo
            if request.method == 'POST':
                cicloToEdit = Cicloescolar.objects.get(idciclo=id)
                cicloToEdit.nombre = request.POST.get('nombre')
                cicloToEdit.fecha_inicio = request.POST.get('fechainicio')
                cicloToEdit.fecha_fin = request.POST.get('fechafin')
                cicloToEdit.fecha_edicion = datetime.now()
                cicloToEdit.status = request.POST.get('status')
                cicloToEdit.save()
                #data['mensaje'] = 'Se ha modificado con exito'
                #data['estilo'] = 'alert alert-success'
                messages.success(request, 'Se modifico correctamente')
                return redirect('/cicloescolar/', data)
                """
                if (Cicloescolar.objects.filter(idciclo=id).exists()):
                    data['ciclo_info'] = Cicloescolar.objects.get(idciclo=id)
                    fechasCiclo = Cicloescolar.objects.get(idciclo=id)
                    fechaInicio = str(fechasCiclo.fecha_inicio)
                    fechaFin = str(fechasCiclo.fecha_fin)
                    data['fechaInicio'] = fechaInicio[0:10]
                    data['fechaFin'] = fechaFin[0:10]
                """
    except Exception as ex:
        print("\nExcepcion en cicloescolarInsert: {0}".format(type(ex).__name__))
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarDelete(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursales(request):
    data = {}
    data['mensaje'] = ''
    data['sucursales'] = Sucursales.objects.filter(status=1)
    return render(request, 'sucursales.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursalesInsert(request):
    data = {}
    data['accion'] = 'agregar'
    data['success'] = 'alert alert-danger'
    data['mostrar'] = False
    data['nombre_sucursal'] = ''
    data['direccion_sucursal'] = ''

    try:
        if request.POST:
            data['nombre_sucursal'] = request.POST['nombre_sucursal']
            data['direccion_sucursal']=request.POST['direccion_sucursal']
            Sucursales.objects.create(
                nombre=data['nombre_sucursal'],
                direccion=data['direccion_sucursal'],
                fecha=timezone.now(),
                fecha_edicion=timezone.now(),
                status='1'
            )
            data['mensaje'] = 'Se ha agregado la sucursal con éxito'
            data['success'] = 'alert alert-success'
            data['mostrar'] = True
    except Exception as ex:
        print("\n\nSe ha detectado una excepcion en sucursalInsert:{}\n\n".format(type(ex).__name__))
        data['mensaje'] = 'Error, no se ha podido agregar la sucursal, intente más tarde'
        data['mostrar'] = True
    else:
        print("\n\nSin excepciones en sucursalInsert\n\n")
    finally:
        print("\n\nTermina sucursalInsert\n\n")

    return render(request, 'sucursales_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursalesUpdate(request, id):
    data = {}
    data['mensaje'] = ''
    data['accion'] = 'editar'
    data['success'] = 'alert alert-danger'
    data['mostrar'] = False
    data['id'] = id
    data['nombre_sucursal'] = ''
    data['direccion_sucursal'] = ''

    try:
        if request.POST and id > 0:
            sucursal = Sucursales.objects.get(idsucursal=id)
            sucursal.nombre = request.POST['nombre_sucursal']
            sucursal.direccion = request.POST['direccion_sucursal']
            sucursal.fecha_edicion = timezone.now()
            sucursal.save()
            data['mensaje'] = 'Se ha editado la sucursal con éxito'
            data['success'] = 'alert alert-success'
            data['mostrar'] = True
        sucursal = Sucursales.objects.get(idsucursal=id)
        data['nombre_sucursal'] = sucursal.nombre
        data['direccion_sucursal'] = sucursal.direccion
    except ObjectDoesNotExist:
        print("\n\nSe ha detectado una excepcion en sucursalUpdate: ObjectDoesNotExist\n\n")
        data['mensaje'] = 'La sucursal que intentas editar, no existe.'
        data['mostrar'] = True
    except Exception as ex:
        print("\n\nSe ha detectado una excepcion en sucursalUpdate:{}\n\n".format(type(ex).__name__))
        data['mensaje'] = 'Error, no se ha podido editar la sucursal, intente más tarde'
        data['mostrar'] = True
    else:
        print("\n\nSin excepciones en sucursalUpdate\n\n")
    finally:
        print("\n\nSaliendo de sucursalUpdate\n\n")

    return render(request, 'sucursales_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursalesDelete(request, id):
    data = {}
    try:

        sucursal = Sucursales.objects.get(idsucursal=id)
        sucursal.status = 0
        sucursal.save()
        data['mensaje'] = 'Se ha eliminado correctamente la sucursal'
    except ObjectDoesNotExist:
        print("\n\nSe ha detectado una excepcion en sucursalDelete: ObjectDoesNotExist\n\n")
        data['No se pudo eliminar la sucursal que desea ya que no se encuentra registrada']

    except Exception as ex:
        print("\n\nSe ha detectado una excepcion en sucursalDelete:{}\n\n".format(type(ex).__name__))
        data['Hubo un error al eliminar la sucursal, intente más tarde.']
    else:
        print("\n\nSin excepciones en sucursalDelete\n\n")
    finally:
        print("\n\nSaliendo de sucursalDelete\n\n")

    return render(request, 'eliminarSucursal.html', data, )

#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def historico(request):
    data = {}
    resultado = []
    data['mensaje'] = ''
    barras = []
    alumnos = Alumnos.objects.values('idgrupo__idciclo__nombre').annotate(cantidad=Count('idgrupo'))
    grupos = Grupos.objects.values('idprofesor__nombres', 'idprofesor__apellido_paterno').annotate(cantidad=Count('idprofesor')).filter(status=1)
    for al in alumnos:
        elemento = {}
        elemento['nombre'] = al['idgrupo__idciclo__nombre']
        elemento['cantidad'] = al['cantidad']
        barras.insert(0, elemento)
    for g in grupos:
        elemento_grupos = {}
        elemento_grupos['nombre'] = g['idprofesor__nombres']
        elemento_grupos['apellido_paterno'] = g['idprofesor__apellido_paterno']
        elemento_grupos['cantidad'] = g['cantidad']
        elemento_grupos['color'] = ('%06X' % randint(0, 0xFFFFFF))
        resultado.insert(0, elemento_grupos)
    data['elementos'] = resultado

    data['ciclos'] = barras
    return render(request, 'historia.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposInsert(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'grupos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def grupos(request):
    data = {}
    data['mensaje'] = ""
    data['pagina'] = 'grupos'
    data['titulo'] = 'Listado de Grupos'
    data['icono'] = 'fa fa-group'
    data['grupos'] = Grupos.objects.filter(status=1)
    return render(request, 'grupos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def clasesInsertar(request):
    data = {}
    data['mensaje'] = ''
    data['stiloRegreso']="display:none"
    try:
        if request.POST:
            id=request.POST['id']
            horainicio = request.POST['horainicio']
            horafin = request.POST['horafin']
            id=Grupos.objects.get(idgrupo=id)
            if horainicio =="" or horafin==" ":
                data['mensaje']="Porfavor llena todos los campos"
                data['estilo']='alert alert-danger'
                data['stiloRegreso']="display:inline"
            else:
                Clases.objects.create(idgrupo=id,fecha=datetime.now(),fecha_edicion=datetime.now(),status=1)
                data['mensaje']="Se ha registrado con exito"
                data['estilo']='alert alert-success'
                data['stiloRegreso']="display:inline"
        else:
            print("No hay Problemas")
    except Exception as ex:
        print("Excepcion en servicios renombrar {}".format(type(ex).__name__))
    else:
        print("Terminado sin ninguna excepcion")
    finally:
        return render(request, 'agregarClase.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def clasesInsert(request,id):
    data = {}
    data['mensaje'] = ''
    data['stiloRegreso']="display:none"
    grupo=Grupos.objects.get(idgrupo=id)
    data['id']= grupo.idgrupo
    data['nombre']=grupo.nombre
    return render(request, 'agregarClase.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def clases(request):
    data = {}
    data['stiloRegreso']="display:none"
    data['mensaje'] = ''
    data['grupos']=Grupos.objects.filter(status=1)
    return render(request, 'clases.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Listar profesores activos
@login_required(login_url='/login/')
def profesores(request):
    data={}
    data['mensaje']=''
    data['pagina'] = 'profesores'
    data['titulo'] = 'Profesores'
    data['icono'] = 'fa fa-male'
    try:
        #status 1 hace referencia a los profesores activos
        data['profesores']=Profesores.objects.filter(status=1) | Profesores.objects.filter(status=-1)
        #status 1 hace referencia a los profesores activos
        data['profesores_activos']=Profesores.objects.filter(status=1).count()
        #status -1 hace referencia a los profesores que estan inactivos
        data['profesores_inactivos']=Profesores.objects.filter(status=-1).count()
        data['total_profesores']=Profesores.objects.filter(status=1).count() + Profesores.objects.filter(status=-1).count()
    except Exception as ex:
        print("Excepcion en servicios renombrar {}".format(type(ex).__name__))
    else:
        print("Termina el metodo profesores desde vistas sin ninguna excepcion")
    finally:
        return render(request,'profesores.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
#Agregar un nuevo profesor
@login_required(login_url='/login/')
def profesoresInsert(request):
    data = {}
    data['mensaje'] = ''
    data['titulo'] = 'Nuevo Profesor'
    data['button'] = "Agregar"
    data['buttonStylo'] = "fa fa-plus-square"
    data['stiloRegreso'] = "display:none"
    data['operacion'] = 'agregar'
    data['icono'] = 'fa fa-mortar-board'
    try:
        if request.POST:
            nombres = request.POST['nombres']
            apellidoPat = request.POST['apellido_pat']
            apellidoMat = request.POST['apellido_mat']
            telefono = request.POST['telefono']
            email = request.POST['email']
            operacion = request.POST['operacion']
            id = request.POST['id']
            if nombres == "" or apellidoPat == "" or apellidoMat == "" or telefono == "" or email == "":
                data['mensaje'] = "Porfavor llena todos los datos"
                data['estilo'] = 'alert alert-danger'
                data['stiloRegreso'] = "display:inline"
                data['operacion'] = operacion
            else:
                if operacion == "agregar":
                    for profesor in Profesores.objects.all():
                        if profesor.nombres.lower() == nombres.lower() and profesor.apellido_paterno.lower() == apellidoPat.lower() and profesor.apellido_materno.lower() == apellidoMat.lower():
                            data['mensaje'] = "El profesor ya existe"
                            data['estilo'] = 'alert alert-danger'
                            data['stiloRegreso'] = "display:inline"
                            return render(request,'profesores_form.html',data,)
                        else:
                            continue
                    Profesores.objects.create(
                        nombres=nombres,
                        apellido_paterno=apellidoPat,
                        apellido_materno=apellidoMat,
                        telefono=telefono,
                        email=email,
                        fecha=datetime.now(),
                        fecha_edicion=datetime.now(),
                        status=1
                    )
                    data['mensaje'] = "Se ha registrado con exito"
                    data['estilo'] = 'alert alert-success'
                    data['stiloRegre so'] = "display:block"
                else:
                    profesor = Profesores.objects.get(idprofesor=id)
                    profesor.nombres = nombres
                    profesor.apellido_paterno = apellidoPat
                    profesor.apellido_materno = apellidoMat
                    profesor.telefono = telefono
                    profesor.email = email
                    profesor.fecha_edicion = datetime.now()
                    profesor.save()
                    data['mensaje'] = "Se ha modificado exitosamente"
                    data['estilo'] = 'alert alert-success'
                    data['stiloRegreso'] = "display:inline"
        else:
            data['operacion'] = "agregar"
    except ObjectDoesNotExist:
        print("Excepcion en vista profesoresInsert: ObjectDoesNotExist")
        data['mensaje'] = "No encontramos algo"
    except Exception as ex:
        print("Excepcion en servicios profesoresInsert {}".format(type(ex).__name__))
    return render(request,'profesores_form.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
#Borrar un profesor
@login_required(login_url='/login/')
def profesoresDelete(request,id):
    data={}
    data['mensaje']=''
    data['leyenda']='Profesor Eliminado Correctamente'
    data['urlorigen']="profesores"
    data['origen']='Regresar a Profesores'
    data['urlinicio']='dashboard'
    data['inicio']='Regresar a Inicio'
    p=Profesores.objects.get(idprofesor=id)
    p.status=0
    p.fecha_edicion=datetime.now()
    p.save()
    return render(request,'eliminarProfesor.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
#Modificar un profesor
@login_required(login_url='/login/')
def profesoresUpdate(request,id):
    data = {}
    data['mensaje'] = ""
    data['titulo'] = "Modificar Profesor"
    data['nombre'] = ''
    data['apellidoPat'] = ''
    data['apellidoMat'] = ''
    data['telefono'] = ''
    data['email'] = ''
    data['button'] = "Guardar"
    data['buttonStylo'] = "fa fa-save"
    data['id'] = id
    data['operacion'] = "editar"

    try:
        profesor = Profesores.objects.get(idprofesor=id)
        if request.method == 'POST':
            data['nombre'] = request.POST['nombres']
            data['apellidoPat'] = request.POST['apellido_pat']
            data['apellidoMat'] = request.POST['apellido_mat']
            data['telefono'] = request.POST['telefono']
            data['email'] = request.POST['email']
            data['button'] = "Guardar"
            data['buttonStylo'] = "fa fa-save"
            data['id'] = id
            data['operacion'] = "editar"
            validaciones = ''
            for field in data:
                if field == '':
                    validaciones =+ 'Por favor ingresa el campo {}\n'.format(field)
            if validaciones == '':
                profesor.nombres = data['nombre']
                profesor.apellido_paterno = data['apellidoPat']
                profesor.apellido_materno = data['apellidoMat']
                profesor.telefono = data['telefono']
                profesor.email = data['email']
                profesor.fecha_edicion = datetime.now(tz=CDMX_TZ)
                profesor.save()
                data['mensaje'] = "Se ha modificado exitosamente"
                data['estilo'] = 'alert alert-success'
                data['stiloRegreso'] = "display:inline"
        profesor = Profesores.objects.get(idprofesor=id)
        data['nombre'] = profesor.nombres
        data['apellidoPat'] = profesor.apellido_paterno
        data['apellidoMat'] = profesor.apellido_materno
        data['telefono'] = profesor.telefono
        data['email'] = profesor.email
    except ObjectDoesNotExist:
        print("\n\nSe detecto una excepcion en profesoresUpdate: ObjectDoesNotExist\n\n")
        data['mensaje'] = 'No se encontró al profesor, por favor intente más tarde.'
    except Exception as ex:
        print("\n\nSe detecto una excepcion en profesoresUpdate:{}\n\n".format(type(ex).__name__))
        data['mensaje'] = 'Hubo un error inesperado, por favor intente más tarde.'
    return render(request,'profesores_form.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def alumnos(request):
    data = {}
    data['mensaje'] = ''
    data['alumnos'] = Alumnos.objects.filter(status=1).order_by('-idalumno')
    data['pagina'] = 'alumnos'
    data['titulo'] = 'Alumnos'
    data['icono'] = 'fa fa-users'
    return render(request, 'alumnos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Ver Alumno#
@login_required(login_url = '/login/')
def alumnosDelete(request,id):
    data = {}
    data['mensaje'] = ''
    data['leyenda']='Alumno Eliminado Correctamente'
    data['urlorigen']='alumnos'
    data['origen']='Regresar a Alumnos'
    data['urlinicio']='dashboard'
    data['inicio']='Regresar a Inicio'
    data['pagina'] = 'alumnos'
    data['titulo'] = 'Alumnos'
    data['icono'] = 'fa fa-users'
    a=Alumnos.objects.get(idalumno=id)
    a.status=0
    a.fecha_edicion=datetime.now()
    a.save()
    return render(request, 'eliminarProfesor.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Ver Alumno#
@login_required(login_url = '/login/')
def alumnosView(request):
    data = {}
    data['mensaje'] = ''
    data['pagina'] = 'alumnos'
    data['titulo'] = 'Alumnos'
    data['icono'] = 'fa fa-users'
    return render(request, 'alumno_ver.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Agregar Alumno#
@login_required(login_url = '/login/')
def alumnosInsert(request):
    data={}
    data['grupos'] = Grupos.objects.filter(status=1)
    data['estilofile'] = "padding: 0px 10px;background: #f5f5f5;color:#555;boder-radius:25px;"
    data['id'] = ''
    data['action'] = 'agregar'
    data['mensaje'] = ''
    data['pagina'] = 'alumnos'
    data['titulo'] = 'Formulario de Alumnos'
    data['icono'] = 'fa fa-users'
    data['url'] = ''
    if request.POST:
        nombres = request.POST['nombres']
        apellido_pat = request.POST['apellidoPaterno']
        apellido_mat = request.POST['apellidoMaterno']
        imagenField= request.FILES['imagen'] if 'imagen' in request.FILES else False
        telefono = request.POST['telefono']
        email = request.POST['email']
        grupo = request.POST['grupo']
        facturaCsos = request.POST['facturaCsos']
        facturaMaterial = request.POST['facturaMaterial']

        if nombres=="" or apellido_pat=="" or apellido_mat==""  or telefono =="" or email=="" or grupo=="" or facturaCsos == "" or facturaMaterial=="":
            data['mensaje']="Por favor , asegurate de no dejar campos vacios, revisa la sección de facturas"
            data['estilo']='alert alert-danger'
            return render(request,'alumnos_form.html',data,)
        else:
            for alumno in Alumnos.objects.all():
                if alumno.nombres.lower() == nombres.lower() and alumno.apellido_paterno.lower() == apellido_pat.lower() and alumno.apellido_materno.lower() == apellido_mat.lower():
                    data['mensaje'] = 'Este alumno ya se encuentra registrado'
                    data['estilo']='alert alert-danger'
                    return render(request,'alumnos_form.html',data,)
                else:
                    continue

        if imagenField:
            validarImg = servicios.validarSizeImg(request.FILES['imagen'].size, request.FILES['imagen'])

            if validarImg == False:
                print("La imagen que subiste no es valida")
                data['mensaje'] = 'La imagen que subiste no cumple con lo siguiente(tamaño menor a 5MB o el formato de la imagen no es valido se aceptan png/jpg/jpeg)'
                data['estilo']='alert alert-warning'
        grupo = Grupos.objects.get(idgrupo=grupo)
        id = Alumnos.objects.create(
            nombres=nombres,
            apellido_paterno=apellido_pat,
            apellido_materno=apellido_mat,
            telefono=telefono,
            email=email,
            idgrupo=grupo,
            numero_factura_csos=facturaCsos,
            numero_factura_material=facturaMaterial,
            fecha=datetime.now(),
            fecha_edicion=datetime.now(),
            status=1
        )
        data['mensaje']='Alumno Registrado Exitosamente'
        data['estilo']='alert alert-success'
        if imagenField:
            alumno = Alumnos.objects.get(
                nombres=nombres,
                apellido_paterno=apellido_pat,
                apellido_materno=apellido_mat
            )
            imagenField = servicios.renombrar(imagen=request.FILES['imagen'], id=id)
            alumno.imagen = imagenField if imagenField != False else ''
            alumno.save()
    return render(request, 'alumnos_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Modificar Alumno#
@login_required(login_url = '/login/')
def alumnosUpdate(request, id):
    data = {}
    data['action'] = 'editar'
    data['id'] = id
    data['estilofile'] = "padding: 0px 10px;background: #f5f5f5;color:#555;boder-radius:25px;"
    data['alumno_info'] = ''
    data['grupos'] = Grupos.objects.filter(status=1)
    data['pagina'] = 'alumnos'
    data['titulo'] = 'Formulario de Alumnos'
    data['icono'] = 'fa fa-users'
    data['mostrar'] = False
    data['mensaje'] = ''
    data['estilo'] = ''
    data['url'] = DIR_MEDIA_ALUMNOS

    try:
        if request.method == 'POST':
            alumnoToEdit = Alumnos.objects.get(idalumno=id)
            alumnoToEdit.nombres = request.POST.get('nombres')
            alumnoToEdit.apellido_paterno = request.POST.get('apellidoPaterno')
            alumnoToEdit.apellido_materno = request.POST.get('apellidoMaterno')
            alumnoToEdit.telefono = request.POST.get('telefono')
            alumnoToEdit.email = request.POST.get('email')
            alumnoToEdit.numero_factura_csos = request.POST.get('facturaCsos')
            alumnoToEdit.numero_factura_material = request.POST.get('facturaMaterial')
            alumnoToEdit.fecha_edicion = datetime.now()
            idgrupo = request.POST.get('grupo')
            alumnoToEdit.idgrupo = Grupos.objects.get(idgrupo=idgrupo)

            imagenField = request.FILES['imagen'] if 'imagen' in request.FILES else False
            if imagenField:
                validarImg = servicios.validarSizeImg(request.FILES['imagen'].size, request.FILES['imagen'])

                if validarImg == False:
                    data['mensaje'] = 'La imagen que subiste no cumple con lo siguiente(tamaño menor a 5MB o el formato de la imagen no es valido se aceptan png/jpg/jpeg)'
                    data['estilo'] = 'alert alert-warning'
                else:
                    imagenField = servicios.renombrar(imagen=request.FILES['imagen'], id=id)
                    alumnoToEdit.imagen = imagenField if imagenField is not False else 'alumnos/Logo.jpg'

            alumnoToEdit.save()
            #messages.success(request, 'Registro modificado correctamente')
            data['mensaje'] = "Actualización exitosa"
            data['estilo'] = 'alert alert-success'
            data['mostrar'] = True
        data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    except Exception as ex:
        print("\nExcepcion en alumnosUpdate: {0}".format(type(ex).__name__))
    return render(request, 'alumnos_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def asistencia(request, id):
    data={}
    data['pagina'] = 'grupos'
    data['titulo'] = 'Asistencias de alumnos'
    data['icono'] = 'fa fa-list-alt'
    data['mostrar'] = False
    data['mensaje'] = ''
    data['estilo'] = ''
    return render(request, 'asistencias.html')
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposUpdate(request):
    data={}
    data['mensaje'] = ''
    return render(request, 'modificar.html', data, )
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def eventos(request):
    data={}
    data['mensaje'] =''
    resultado = []

    grupos=Grupos.objects.filter(status=1)
    for grupo in grupos:

        elementos = {}
        #for profesor in profesores:
        elementos['id']=grupo.idgrupo
        elementos['title']=grupo.nivel
        elementos['end']=grupo.termina
        elementos['start']=grupo.inicia
        elementos['notas']=grupo.notas
        elementos['horario']=grupo.horario
        elementos['apellidoPaterno']=grupo.idprofesor.apellido_paterno
        elementos['nombre']=grupo.idprofesor.nombres
        elementos['apellidoMaterno']=grupo.idprofesor.apellido_materno
        resultado.insert(0,elementos)
    from pprint import pprint
    from pprint import pprint
    pprint(resultado)
    data['grupos'] = resultado
    return JsonResponse(data)

@login_required(login_url='/login/')
def profesoresPagos(request):
    data = {}
    data['mensaje']=''
    data['profesores'] = Profesores.objects.filter(status=1)
    return render(request, 'pagoProfesor.html', data, )
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def pagosProfesorAgregar(request,id):
    data={}
    data['mensaje']=''
    data['accion'] = 'agregar'
    data['id'] = id
    data['profesores'] = Profesores.objects.filter(status=1, idprofesor=id)
    data['concepto'] = ''
    data['fecha_pago'] = ''
    data['descripcion'] = ''
    data['pago'] = ''
    data['iva'] = ''
    data['mostrar'] = False
    data['success'] = ''
    try:
        if not data['profesores']:
            data['mensaje'] = 'Profesor no encontrado'
            data['mostrar'] = True
            data['success'] = 'alert alert-danger'

        if request.method == 'POST':
            profesor = Profesores.objects.filter(status=1, idprofesor=request.POST['profesor'])
            if profesor:
                data['concepto'] = request.POST['concepto']
                data['fecha_pago'] = request.POST['fecha_pago']
                data['descripcion'] = request.POST['descripcion']
                data['pago'] = request.POST['pago']
                data['iva'] = 1 if 'iva' in request.POST else 0
                PagoProfesores.objects.create(
                    idprofesor=Profesores.objects.get(idprofesor=request.POST['profesor']),
                    concepto=data['concepto'],
                    iva=data['iva'],
                    descripcion=data['descripcion'],
                    pago=data['pago'],
                    fecha_pago=data['fecha_pago'],
                    fecha_edicion=datetime.now(tz=CDMX_TZ).strftime("%Y-%m-%d %H:%M:%S"),
                    fecha=datetime.now(tz=CDMX_TZ).strftime("%Y-%m-%d %H:%M:%S"),
                    status=1
                )
                data['mensaje'] = 'Se ha agregado el pago para este profesor'
                data['mostrar'] = True
                data['success'] = 'alert alert-success'
            else:
                data['mensaje'] = 'Profesor no encontrado, no se registró el pago'
                data['mostrar'] = True
                data['success'] = 'alert alert-danger'
    except Exception as ex:
        print('\nSe ha detectado una excepción en pagosAgregar:{}'.format(type(ex).__name__))
        data['mensaje'] = 'Hubo un error al momento de registrar el pago, intentalo más tarde'
        data['mostrar'] = True
        data['success'] = 'alert alert-danger'
    return render(request,'pagoProfesor_form.html', data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def pagosProfesorHistorial(request, id):
    data={}
    data['mensaje']=''
    data['accion'] = 'editar'
    data['id'] = id
    data['profesor'] = Profesores.objects.get(status=1, idprofesor=id)
    data['pagos'] = PagoProfesores.objects.filter(status=1,idprofesor=id)
    return render(request,'pagoProfesorHistorial.html', data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def facturacion(request,id=''):
    data={}
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    data['id'] = id
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    data['materiales'] = Materiales.objects.filter(idalumno=id)
    data['colegiaturas'] = Colegiaturas.objects.filter(idalumno=id)
    data['mensaje'] = ''
    return render(request, 'facturacion.html', data)

@login_required(login_url = '/login/')
def listaGrupo(request):
    data={}
    data['grupos'] = Grupos.objects.filter(status=1)
    data['mensaje'] = ''
    return render(request, 'listagrupo.html',data)

#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def pagoAlumnos(request, id = ''):
    data = {}
    data['mensaje'] = ''
    data['id'] = id
    data['materiales']=Materiales.objects.all().select_related('idalumno').filter(idgrupo=id)
    data['alumnos']=Alumnos.objects.filter(idgrupo=id)
    return render(request, 'pagoalumnos.html',data)


#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def generarExcel(request):
    data = {}
    data['mensaje'] = ''
    data['grupos'] = Grupos.objects.filter(status=1)
    return render(request, 'generarExcel.html', data, )
#-----------------------------------------------------------------------------------------------------------------------#
def generar(request,id):
    data={}
    data['mensaje']=''
    return servicios.generarHoja(id=id)

@login_required(login_url='/login/')
def gruposList(request, id):
    data = {}
    data ['grupos']= Grupos.objects.all()
    data ['mensaje'] = "p"

    return render(request, 'grupos_form.html', data, )
# -----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposInsert(request):
    data={}
    data['accion'] = 'agregar'
    data['id'] = 0
    data['grupos'] = Grupos.objects.filter(status=1)
    data['mensaje'] = ''
    data['profesores'] = Profesores.objects.filter(status=1)
    data['sucursales'] = Sucursales.objects.filter(status=1)
    data['ciclos'] = Cicloescolar.objects.all()
    data['pagina'] = 'grupos'
    data['titulo'] = 'Agregar Grupo'
    data['icono'] = 'fa fa-group'
    data['mostrar'] = False
    if request.method == 'POST':
        nivel = request.POST['nivel']
        profesores = request.POST['profesores']
        sucursales = request.POST['sucursales']
        cicloescolar = request.POST['cicloescolar']
        fechainicio = request.POST['fechainicio']
        fechafin = request.POST['fechafin']
        horario = request.POST['horario']
        notas = request.POST['notas']
        nombre = request.POST['nombre']
        aula = request.POST['aula']
        tipo_grupo = request.POST['tipo']

        if nivel == "" or profesores == "" or sucursales == "" or cicloescolar == "" or fechainicio == "" or fechafin == "" or horario == "" or notas == "" or nombre == "" or aula == "" or tipo_grupo == "":
            data['mensaje'] = "Asegurese de que los campos esten llenos."
            data['estilo'] = "alert alert-danger"

        else:
            for grupo in Grupos.objects.all():
                if grupo.nivel == nivel:
                    data['mensaje'] = "Este grupo ya esta registrado."
                    data['estilo'] = "alert alert-danger"
                else:
                    continue
            sucursal = Sucursales.objects.get(idsucursal=sucursales)
            profesor = Profesores.objects.get(idprofesor=profesores)
            ciclo = Cicloescolar.objects.get(idciclo=cicloescolar)
            Grupos.objects.create(
                nivel=nivel,
                idprofesor=profesor,
                idsucursal=sucursal,
                idciclo=ciclo,
                inicia=fechainicio,
                termina=fechafin,
                horario=horario,
                notas=notas,
                nombre=nombre,
                aula=aula,
                status=1,
                tipo_grupo=tipo_grupo
            )

            data['estilo'] = "alert alert-success"
            data['mensaje'] = 'Se a Añadio el grupo correctamente'
            data['mostrar'] = True
    return render(request, 'grupos_form.html', data, )
# -----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposUpdate(request, id):
    data={}
    data['accion'] = 'editar'
    data['id'] = int(id)
    data['grupo_info'] = Grupos.objects.get(idgrupo=id)
    data['pagina'] = 'grupos'
    data['profesores'] = Profesores.objects.filter(status=1)
    data['sucursales'] = Sucursales.objects.filter(status=1)
    data['ciclos'] = Cicloescolar.objects.all()
    data['grupos'] = Grupos.objects.filter(status=1)
    data['pagina'] = 'grupos'
    data['titulo'] = 'Editar Grupo'
    data['icono'] = 'fa fa-group'
    if request.method == 'POST':
        nivel = request.POST['nivel']
        profesores = request.POST['profesores']
        sucursales = request.POST['sucursales']
        cicloescolar = request.POST['cicloescolar']
        fechainicio = request.POST['fechainicio']
        fechafin = request.POST['fechafin']
        horario = request.POST['horario']
        notas = request.POST['notas']
        nombre = request.POST['nombre']
        aula = request.POST['aula']
        tipo_grupo = request.POST['tipo']

        grupos=Grupos.objects.get(idgrupo=id)
        grupos.nivel = nivel
        grupos.idprofesor = Profesores.objects.get(idprofesor=profesores)
        grupos.idsucursal = Sucursales.objects.get(idsucursal=sucursales)
        grupos.idciclo = Cicloescolar.objects.get(idciclo=cicloescolar)
        grupos.inicia = fechainicio
        grupos.termina = fechafin
        grupos.horario = horario
        grupos.notas = notas
        grupos.nombre = nombre
        grupos.aula = aula
        grupos.tipo_grupo = tipo_grupo
        grupos.save()
        data['estilo'] = "alert alert-success"
        data['mensaje'] = 'Se a editado el grupo correctamente'
        data['mostrar'] = True

    data['grupo_info'] = Grupos.objects.get(idgrupo=id)
    return render(request, 'grupos_form.html', data, )
# -----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposDelete(request,id):
    data = {}
    data['mensaje'] = "Grupo Eliminado"
    data['mensaje']=''
    data['leyenda']='Grupo Eliminado Correctamente'
    data['urlorigen']="grupos"
    data['origen']='Regresar a Grupos'
    data['urlinicio']='dashboard'
    data['inicio']='Regresar a Inicio'
    grupo = Grupos.objects.get(idgrupo=id)
    grupo.status = 0
    grupo.save()
    return render(request, 'eliminarGrupo.html', data, )
# -----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def reporte(request):
    data={}
    data['alumnos']=Alumnos.objects.all()
    data['mensaje'] = ''
    data['grupos']=Grupos.objects.all()
    data['ciclos']=Cicloescolar.objects.all()
    return render(request,'reporte.html',data)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def colegiatura(request, id=''):
    data={}
    data['id'] = id
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    return render(request, 'colegiatura.html',data)
@login_required(login_url = '/login/')
#-----------------------------------------------------------------------------------------------------------------------#
def compraMaterial(request, id=''):
    data={}
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    data['id'] = id

    return render(request, 'compra.html',data)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def materialInsert(request, id=''):
    data = {}
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    data['id'] = id
    data['alumno_info'] = Alumnos.objects.get(idalumno=id)
    alumno_info=data['alumno_info']
    data['materiales']=Materiales.objects.all()
    if request.method == 'POST':
        nombre=request.POST['nombre']
        costo=request.POST['costo']
        metodo=request.POST['metodo']
        pago=request.POST['pago']
        if nombre=="" or costo=="" or metodo=="" or pago=="":
            data['mensaje']="Los campos deben estar llenos"
            data['estilo']="alert alert-danger"
        else:

            for material in Materiales.objects.all():
                if material.nombre==nombre:
                    data['mensaje']="Ya se encuentra registrado"
                    data['estilo']="alert alert-danger"
                else:
                    continue
            idalumno=Alumnos.objects.get(idalumno=id)
            Materiales.objects.create(idalumno=idalumno ,nombre=nombre, costo=costo, metodo=metodo,pago=pago,fecha=datetime.now())
            data['estilo']="alert alert-success"
            messages.success(request, 'Se añadido el material correctamente')




    return render(request, 'formMaterial.html',data)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def colegiaturaInsert(request, id):
    data={}
    data['mensaje'] = ''
    data['id'] = id
    return render(request, 'colegiatura.html',data)
