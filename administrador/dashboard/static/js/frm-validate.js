$().ready(function () {
    $.validator.setDefaults({
        errorElement: "em",
        errorPlacement: function (error, element) {
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                var errorEl = element;

                if (element.parent('.input-group').length > 0) {
                    errorEl = element.parent('.input-group');
                }

                error.insertAfter(errorEl);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group" )
                .addClass( "has-error" )
                .removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".form-group")
                .addClass( "has-success" )
                .removeClass( "has-error" );
        }
    });
    // Form alumnosUpdate
    if ($('.frm-alumno').length > 0) {
        $('.frm-alumno').validate({
            rules: {
                nombres: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                },
                apellidoPaterno: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                },
                apellidoMaterno: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                },
                telefono: {
                    required: true,
                    minlength: 8,
                    maxlength: 15
                },
                email: {
                    required: true,
                    minlength: 1,
                    maxlength: 50,
                    email: true
                },
                facturaCsos: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                },
                facturaMaterial: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                }
            }
        });
    }
});