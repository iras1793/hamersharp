$(function() { 
  $('#insertar').on('click',function(){ 
      $('#formulario').validate({ 
           rules: {
                        nivel: {
                            required: true,
                            rangelength: [5, 40],
                        },
                        profesores: {
                            required: true,
                            rangelength: [5, 40],

                        },
                        sucursales: {
                            required: true,
                            rangelength: [5, 40],
                        },
                        ciclo: {
                            required: true,
                            rangelength: [5, 40],
                        },
                        horario:{
                            required: true,
                            rangelength: [0, 4],
                            numeric: true
                        },
                        nombre:{
                            required: true,
                            rangelength: [5, 40],

                        },
                        aula:{
                            required: true,
                            rangelength: [5, 40],
                        },
                        notas:{
                            required: true,
                            rangelength: [5, 40],
                    },
                },
                messages: {
                   nivel: {
                        required: "Necesita insertar un nivel",
                        rangelength: "Revise que el rango de carateres sea de 4 a 100 caracteres",
                    },
                    profesores: {
                        required: "Necesita seleccionar un profesor",
                        rangelength: "Revise que el rango de carateres sea de 4 a 100 caracteres",

                    },
                    sucursales: {
                        required: "Necesita seleccionar un profesor",
                        rangelength: "Revise que el rango de carateres sea de 4 a 100 caracteres",
                    },
                    ciclo: {
                        required: "Necesita seleccionar un profesor",
                        rangelength: "Revise que el rango de carateres sea de 4 a 100 caracteres",
                    },
                    horario:{
                        required: "Necesita tener un horario registrado",
                        rangelength: "Sobrepaso el numero de caracteres, asegurese de que tenga el formato: 7:00 - 10:00",
                        numeric: "Este campo solo requiere números no letras"
                    },
                    nombre:{
                        required: "Este campo es obligatorio",
                        rangelength: "Solo introducir de un minimo de 5 caracteres a 50 caracteres",

                    },
                    aula:{
                        required: "Campo requerido",
                        rangelength: "Solo introducir de un minimo de 2 caracteres a 5 caracteres",
                    },
                    notas:{
                        required: "Campo requerido",
                        rangelength: "Agregue un comentario de 10 a 100 caracteres",
                    },
                },
      }); 
   }); 
}); 

//if(fecha > DateTime.Now && fecha < DateTime.Now){

//}