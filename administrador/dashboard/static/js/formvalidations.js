$(function() {
  $("#insertar").on("click",function(){
      $( "#formulario" ).validate({
           rules: {
                   nombres: {
                           required: true,
                           minlength: 3,
                           maxlength: 20
                   },
                   apellido_pat:{
                          required:true,
                          minlength:3,
                          maxlength:20
                   },
                   apellido_mat:{
                          required:true,
                          minlength:3,
                          maxlength:20
                   },
                   telefono:{
                          required:true,
                          minlength:8,
                          maxlength:11,
                          digits:true
                   },
                   email:{
                          required: true,
                          email: true
                  },
                  facturaCsos:{
                          required:true,
                          minlength:5,
                          maxlength:15
                  },
                  facturaMaterial:{
                          required:true,
                          minlength:5,
                          maxlength:15
                  },
                  imagen:{
                      required:true
                  }
           },
           messages: {
                   nombres: {
                           required: "Ingresa tu nombre",
                           minlength: jQuery.validator.format("Al menos 3 caracteres"),
                           maxlength: jQuery.validator.format("Solo admito 20 caracteres")
                   },
                   apellido_pat:{
                          required:"Ingresa tu Apellido Paterno",
                          minlength:jQuery.validator.format("Al menos 3 caracteres"),
                          maxlength:jQuery.validator.format("Al menos 20 caracteres")
                   },
                   apellido_mat:{
                          required:"Ingresa tu Apellido Paterno",
                          minlength:jQuery.validator.format("Al menos 3 caracteres"),
                          maxlength:jQuery.validator.format("Al menos 20 caracteres")
                   },
                   telefono:{
                          required:"Ingresa tu numero telefonico",
                          minlength:jQuery.validator.format("Al menos 8 caracteres"),
                          maxlength:jQuery.validator.format("Solo admito 11 caracteres"),
                          digits:"Solo numeros"
                   },
                   email:{
                     required:"Ingresa tu email",
                     email:"Ingresa un correo correcto"
                   },
                   facturaCsos:{
                           required:"Ingresa el numero de Csos",
                           minlength:jQuery.validator.format("Al menos 5 caracteres"),
                           maxlength:jQuery.validator.format("Solo se admiten 15 caracteres")
                   },
                   facturaMaterial:{
                           required:"Ingresa en Numero de factura Material",
                           minlength:jQuery.validator.format("Al menos 5 caracteres"),
                           maxlength:jQuery.validator.format("Solo se admiten 15 caracteres")
                   },
                   imagen:{
                       required:"Ingresa una imagen",
                   }

           }
      });
   });
});
