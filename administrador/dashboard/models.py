from django.db import models
from django.utils import timezone


class Alumnos(models.Model):
    idalumno = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellido_paterno = models.CharField(max_length=100, blank=True, null=True)
    apellido_materno = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    examen_1 = models.FloatField(blank=True, null=True)
    examen_2 = models.FloatField(blank=True, null=True)
    examen_3 = models.FloatField(blank=True, null=True)
    calificacion_final = models.FloatField(blank=True, null=True)
    numero_factura_csos = models.CharField(max_length=100, blank=True, null=True)
    suma_montos_csos = models.CharField(max_length=100, blank=True, null=True)
    numero_factura_material = models.CharField(max_length=100, blank=True, null=True)
    ingreso_total = models.CharField(max_length=100, blank=True, null=True)
    imagen = models.ImageField(upload_to ='alumnos/',max_length=100, blank=True, null=True)
    idgrupo = models.ForeignKey('Grupos', models.DO_NOTHING, db_column='idgrupo', blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True,default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True,default=timezone.now)
    status = models.IntegerField(blank=True, null=True,default=1)

    def __str__(self):
        return str(self.idalumno)

    class Meta:
        managed = True
        db_table = 'alumnos'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Cicloescolar(models.Model):
    idciclo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True,default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True,default=timezone.now)
    status = models.IntegerField(blank=True, null=True,default=1)

    class Meta:
        managed = True
        db_table = 'cicloescolar'


class Grupos(models.Model):
    idgrupo = models.AutoField(primary_key=True)
    idprofesor = models.ForeignKey('Profesores', models.DO_NOTHING, db_column='idprofesor', blank=True, null=True)
    idsucursal = models.ForeignKey('Sucursales', models.DO_NOTHING, db_column='idsucursal', blank=True, null=True)
    idciclo = models.ForeignKey(Cicloescolar, models.DO_NOTHING, db_column='idciclo', blank=True, null=True)
    tipo_grupo = models.CharField(max_length=100, blank=True, null=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    aula = models.CharField(max_length=100, blank=True, null=True)
    horario = models.CharField(max_length=100, blank=True, null=True)
    inicia = models.DateField(blank=True, null=True)
    termina = models.DateField(blank=True, null=True)
    nivel = models.CharField(max_length=100, blank=True, null=True)
    notas = models.TextField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True, default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    def __str__(self):
        return self.idgrupo

    class Meta:
        managed = True
        db_table = 'grupos'


class Asistencias(models.Model):
    idasistencia = models.AutoField(primary_key=True)
    idgrupo = models.ForeignKey(Grupos, models.DO_NOTHING, db_column='idgrupo', blank=True, null=True)
    idalumno = models.ForeignKey(Alumnos, models.DO_NOTHING, db_column='idalumno', blank=True, null=True)
    fecha_asistencia = models.DateField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True, default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    class Meta:
        managed = True
        db_table = 'asistencias'


class Colegiaturas(models.Model):
    idcolegiatura = models.AutoField(primary_key=True)
    idalumno = models.ForeignKey(Alumnos, models.DO_NOTHING, db_column='idalumno', blank=True, null=True)
    idgrupo = models.ForeignKey(Grupos, models.DO_NOTHING, db_column='idgrupo', blank=True, null=True)
    costo = models.CharField(max_length=5, blank=True, null=True)
    metodo = models.CharField(max_length=20, blank=True, null=True)
    pago = models.CharField(max_length=20, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    def __str__(self):
        return self.idcolegiatura

    class Meta:
        managed = True
        db_table = 'colegiatura'


class Materiales(models.Model):
    idmaterial = models.AutoField(primary_key=True)
    idalumno = models.ForeignKey(Alumnos, models.DO_NOTHING, db_column='idalumno', blank=True, null=True)
    idgrupo = models.ForeignKey(Grupos, models.DO_NOTHING, db_column='idgrupo', blank=True, null=True)
    nombre = models.CharField(max_length=255, blank=True, null=True)
    costo = models.CharField(max_length=5, blank=True, null=True)
    metodo = models.CharField(max_length=20, blank=True, null=True)
    pago = models.CharField(max_length=20, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    class Meta:
        managed = True
        db_table = 'material'



class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

class Profesores(models.Model):
    idprofesor = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellido_paterno = models.CharField(max_length=100, blank=True, null=True)
    apellido_materno = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True, default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    def __str__(self):
        return self.nombres

    class Meta:
        managed = True
        db_table = 'profesores'

class Clases(models.Model):
    idclase = models.AutoField(primary_key=True)
    idgrupo = models.ForeignKey('Grupos', models.DO_NOTHING, db_column='idgrupo', blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True, default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    def __str__(self):
        return self.idgrupo

    class Meta:
        managed = True
        db_table = 'clases'


class Sucursales(models.Model):
    idsucursal = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True, default=timezone.now)
    fecha_edicion = models.DateTimeField(blank=True, null=True, default=timezone.now)
    status = models.IntegerField(blank=True, null=True, default=1)

    class Meta:
        managed = True
        db_table = 'sucursales'

class PagoProfesores(models.Model):
    idpagoprofesor = models.AutoField(primary_key=True)
    idprofesor = models.ForeignKey(Profesores, models.DO_NOTHING, db_column='idprofesor', blank=True, null=True)
    concepto = models.CharField(max_length=100, blank=True, null=False, default='')
    iva = models.IntegerField(blank=False, null=False, default=0)
    descripcion = models.TextField(max_length=200, blank=True, null=False,default='')
    pago = models.FloatField(blank=False, null=False,default=0.0)
    fecha_pago = models.DateField(blank=False, null=False)
    fecha_edicion = models.DateTimeField(blank=True, null=True)
    fecha = models.DateTimeField(blank=False, null=False)
    status = models.IntegerField(blank=False, null=False, default=1)

    class Meta:
        managed = True
        db_table = 'pagos_profesores'

